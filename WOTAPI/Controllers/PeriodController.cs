using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class PeriodController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        // GET: api/Period
        public ListPeriodModel GetPeriodes()
        {
            var item = new ListPeriodModel()
            {
                PeriodList = (from m in db.Periodes
                              orderby m.DateDebut descending
                              select m).ToList(),
            };
            return (item);
        }

        // POST: api/Period
        public IHttpActionResult PostPeriod(PeriodModel model)
        {
            var periode = new Periode();

            var chevauchement = db.Periodes.Any(x => model.DateDebut > x.DateDebut && model.DateDebut < x.DateFin
              || model.DateFin > x.DateDebut && model.DateFin < model.DateFin
              || model.DateDebut < x.DateDebut && model.DateFin > x.DateFin
              || model.DateDebut > x.DateDebut && model.DateFin < x.DateFin);

            if (chevauchement)
            {
                return (null);
            }

            if (ModelState.IsValid)
            {
                periode = new Periode 
                {
                    NomPeriode = model.NomPeriode,
                    DateDebut = model.DateDebut,
                    DateFin = model.DateFin,
                };

                db.Periodes.Add(periode);
                db.SaveChanges();

            }

            return Ok(periode);
        }

        //public HttpResponseMessage PeriodPodium()
        //{
        //    string currentUserId = User.Identity.GetUserId();
        //    Users utilisateurConnecte = db.Users.Find(currentUserId);

        //    var equipeUsers = db.Users.Where(y => y.EquipeId == utilisateurConnecte.EquipeId).Select(y => y.Id).ToList();


        //    var periode = db.Periodes.FirstOrDefault(x => DateTime.Now < x.DateFin && DateTime.Now > x.DateDebut);


        //    if (periode == null)
        //    {
        //        return null
        //            ;
        //    }

        //    List<IGrouping<Users, ShameUser>> groupementShamePerUser = db.ShameUsers
        //        .Where(z => z.Statut == ShameUserStatut.valide && z.DateShame < periode.DateFin && z.DateShame > periode.DateDebut && equipeUsers.Contains(z.UserId))
        //        .GroupBy(x => x.User)
        //        .OrderByDescending(y => y.Count()).ThenBy(x => x.Key.Prenom)
        //        .Take(3)
        //        .ToList();

        //    List<UserModel> userViewModels = groupementShamePerUser.Select(
        //        g =>
        //            new UserModel
        //            {
        //                ShamesList =
        //                        g.Select(s => new ShameUserModel { ShameUserId = s.ShameUserId, Statut = s.Statut })
        //                                .ToList(),

        //                UserId = g.Key.Id,
        //                UserName = g.Key.Prenom,
        //            }).ToList();


        //    return Request.CreateResponse(HttpStatusCode.OK, userViewModels);

        //}

        // PUT: api/Period/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPeriod(int id, Periode model)
        {
            var thisShame = db.Periodes.Find(model.PeriodeId);

            if (ModelState.IsValid)
            {
                thisShame.NomPeriode = model.NomPeriode;
                thisShame.DateDebut = model.DateDebut;
                thisShame.DateFin = model.DateFin;
                //db.Entry(shame).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Ok(thisShame);

            if (id != model.PeriodeId)
            {
                return (null);
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShameExists(id))
                {
                    return (null);
                }
                else
                {
                    throw;
                }
            }

            return (null);
        }

        // DELETE: api/Shames/5
        [ResponseType(typeof(Shame))]
        public HttpResponseMessage DeleteShame(int id)
        {
            Shame shame = db.Shames.Find(id);
            if (shame == null)
            {
                return (null);
            }

            db.Shames.Remove(shame);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, shame);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShameExists(int id)
        {
            return db.Shames.Count(e => e.Id == id) > 0;
        }
    }
}