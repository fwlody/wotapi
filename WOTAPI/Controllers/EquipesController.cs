﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class EquipesController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        //GET: api/Equipes
        public List<Equipe> GetEquipes()
        {
            var equipes = db.Equipes.ToList();
            return equipes;
        }


        //public List<Users> GetMembersTeam()
        //{
        //    var currentId = User.Identity.GetUserId();
        //    Users utilisateurConnecte = db.Users.Find(currentId);

        //    var model = db.Users.Where(e => e.EquipeId == utilisateurConnecte.EquipeId);
        //    return model.ToList();

        //}

        // GET: api/Equipes/5
        [ResponseType(typeof(Equipe))]
        public HttpResponseMessage GetEquipe(int id)
        {
            Equipe equipe = db.Equipes.Find(id);
            if (equipe == null)
            {
                return (null);
            }

            return Request.CreateResponse(HttpStatusCode.OK, equipe);
        }

        // PUT: api/Equipes/5
        [ResponseType(typeof(void))]
        public HttpResponseMessage PutEquipe(int id, Equipe model)
        {
            if (!ModelState.IsValid)
            {
                var thisShame = db.Equipes.Find(model.EquipeId);
                thisShame.NomEquipe = model.NomEquipe;
                thisShame.PhotoEquipe = model.PhotoEquipe;
                thisShame.User = model.User;
            }

            if (id != model.EquipeId)
            {
                return (null);
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EquipeExists(id))
                {
                    return (null);
                }
                else
                {
                    throw;
                }
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST: api/Equipes
        [ResponseType(typeof(Equipe))]
        public HttpResponseMessage PostEquipe(Equipe model)
        {
            byte[] imageConvertie = new byte[] { };

            //if (model.PhotoEquipe != null)
            //{
            //    using (var item = new MemoryStream())
            //    {
            //        model.PhotoEquipe.InputStream.CopyTo(item);
            //        imageConvertie = item.ToArray();
            //    }
            //}
            if (ModelState.IsValid)
            {
                var equipe = new Equipe()
                {
                    NomEquipe = model.NomEquipe,
                    PhotoEquipe = imageConvertie,
                    User = new List<User>(),
                };

                db.Equipes.Add(equipe);
                db.SaveChanges();

            }
            return Request.CreateResponse(HttpStatusCode.OK, model);
        }

        // DELETE: api/Equipes/5
        [ResponseType(typeof(Equipe))]
        public HttpResponseMessage DeleteEquipe(int id)
        {
            Equipe equipe = db.Equipes.Find(id);
            if (equipe == null)
            {
                return (null);
            }

            db.Equipes.Remove(equipe);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, equipe);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EquipeExists(int id)
        {
            return db.Equipes.Count(e => e.EquipeId == id) > 0;
        }
    }
}