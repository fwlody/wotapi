﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class AccountController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        public HttpResponseMessage Profil()
        {
            string currentUserId = User.Identity.GetUserId();
            var model = db.Users.Where(x => x.Id == currentUserId)
                .Select(u => new UserModel
                {
                    UserId = u.Id,
                    UserName = u.Prenom,
                    UserLastName = u.Nom,
                    ShamesList =
                        u.ShameUsers.Where(
                                x => x.Statut == ShameUserStatut.propose || x.Statut == ShameUserStatut.valide)
                                .GroupBy(shame => shame.Shame)
                            .Select(x => new ShameUserModel()
                            {
                                Nom = x.Key.Nom,
                                Description = x.Key.Description,
                                Date = x.Key.Date,
                                ImageOriginal = x.Key.Image,
                                Valides = x.Count(y => y.Statut == ShameUserStatut.valide),
                                Proposes = x.Count(y => y.Statut == ShameUserStatut.propose),
                                Refuses = x.Count(y => y.Statut == ShameUserStatut.refuse),
                            }).ToList()
                })
                .OrderBy(u => u.UserName).ToList();

            model = model.Where(x => x.ShamesList.Any()).ToList();

            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}
