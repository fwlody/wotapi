﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WOTAPI.Models;


namespace WOTAPI.Controllers
{
    public class AdminController : ApiController
    {
        public IHttpActionResult Create(CreateUserModel model)
        {

            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Email,
                    Email = model.Email,
                    Prenom = model.Prenom,
                    Nom = model.Nom,
                    EquipeId = model.EquipeId

                };
            }
            return Ok(model);
        }
    }
}
