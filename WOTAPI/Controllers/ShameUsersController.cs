﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class ShameUsersController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        // GET: api/ShameUsers
        public IList<UserModel> GetShameUsers()
        {
            string currentUserName = User.Identity.Name;
            User utilisateurConnecte = db.Users.Single(x => x.UserName == currentUserName)
                ;
            var model = db.Users.Where(y => y.EquipeId == utilisateurConnecte.EquipeId)
                    .Select(u => new UserModel
                    {
                        UserId = u.Id,
                        UserName = u.Prenom,
                        ShamesList = u.ShameUsers.Where(x => x.Votes.All(y => y.Createur != utilisateurConnecte.Id)
                                                             && (x.Statut == ShameUserStatut.propose))
                            .Select(x => new ShameUserModel()
                            {
                                Nom = x.Shame.Nom,
                                Description = x.Shame.Description,
                                Date = x.DateShame,
                                ImageOriginal = x.Shame.Image,
                                ShameUserId = x.ShameUserId,
                                Statut = x.Statut,
                                VoteOui = x.Votes.Count(vote => vote.Valeur == true),
                                VoteNon = x.Votes.Count(vote => vote.Valeur == false)
                            }).ToList()
                    })
                    .OrderBy(u => u.UserName).ToList();

            model = model.Where(x => x.ShamesList.Any()).ToList();

            return model;
        }

        // GET: api/ShameUsers/5
        [ResponseType(typeof(ShameUser))]
        public IHttpActionResult GetShameUser(int id)
        {
            ShameUser shameUser = db.ShameUsers.Find(id);
            if (shameUser == null)
            {
                return NotFound();
            }

            return Ok(shameUser);
        }

        // PUT: api/ShameUsers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShameUser(int id, ShameUser shameUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != shameUser.ShameUserId)
            {
                return BadRequest();
            }

            db.Entry(shameUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShameUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ShameUsers
        [ResponseType(typeof(ShameUser))]
        public IHttpActionResult PostShameUser(AddShameUserModel model)
        {
            string currentUserName = User.Identity.Name;
            User utilisateurConnecte = db.Users.Single(x => x.UserName == currentUserName)
                ;

            var item = new ShameUser();

            if (ModelState.IsValid)
            {
                item = new ShameUser
                {
                    ShameId = model.ShameId,
                    UserId = model.UserId,
                    DateShame = DateTime.Now,
                    Createur = utilisateurConnecte.Id,
                    Statut = ShameUserStatut.propose
                };

                db.ShameUsers.Add(item);
                db.SaveChanges();
            }
            return Ok(item);
        }

        // DELETE: api/ShameUsers/5
        [ResponseType(typeof(ShameUser))]
        public IHttpActionResult DeleteShameUser(int id)
        {
            ShameUser shameUser = db.ShameUsers.Find(id);
            if (shameUser == null)
            {
                return NotFound();
            }

            db.ShameUsers.Remove(shameUser);
            db.SaveChanges();

            return Ok(shameUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShameUserExists(int id)
        {
            return db.ShameUsers.Count(e => e.ShameUserId == id) > 0;
        }
    }
}