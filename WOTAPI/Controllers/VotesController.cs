﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    [Authorize]
    public class VotesController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        // GET: api/Votes
        public IQueryable<Vote> GetVotes()
        {
            var votes = db.Votes.AsQueryable();
            return votes;
            //string currentUserId = User.Identity.GetUserId();
            //Users utilisateurConnecte = db.Users.Find(currentUserId);

            //var model = db.Users.Where(y => y.EquipeId == utilisateurConnecte.EquipeId)
            //        .Select(u => new UserModel
            //        {
            //            UserId = u.Id,
            //            UserName = u.Prenom,
            //            ShamesList = u.ShameUsers.Where(x => x.Votes.All(y => y.Createur != currentUserId)
            //                                                 && (x.Statut == ShameUserStatut.propose))
            //                .Select(x => new ShameUserModel()
            //                {
            //                    Nom = x.Shame.Nom,
            //                    Description = x.Shame.Description,
            //                    Date = x.DateShame,
            //                    ImageOriginal = x.Shame.Image,
            //                    ShameUserId = x.ShameUserId,
            //                    Statut = x.Statut,
            //                    VoteOui = x.Votes.Count(vote => vote.Valeur == true),
            //                    VoteNon = x.Votes.Count(vote => vote.Valeur == false)
            //                }).ToList()
            //        })
            //        .OrderBy(u => u.UserName).ToList();

            //model = model.Where(x => x.ShamesList.Any()).ToList();

            //return model;
        }

        // GET: api/Votes/5
        [ResponseType(typeof(Vote))]
        public IHttpActionResult GetVote(int id)
        {
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return NotFound();
            }

            return Ok(vote);
        }

        // POST: Voter
        [HttpPost]
        public HttpResponseMessage PostVote(VoteModel model, string id)
        {
            string currentUserName = User.Identity.Name;
            User utilisateurConnecte = db.Users.Single(x=> x.UserName == currentUserName)
                ;


            if (ModelState.IsValid)
            {

                var item = new Vote()
                {
                    Valeur = model.Valeur,
                    Createur = utilisateurConnecte.Id,
                    ShameUserId = model.ShameUserId,
                    DateCreation = DateTime.Now,
                };

                db.Votes.Add(item);
                db.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK, model);

            //var compteur = (from v in db.Votes
            //                where v.ShameUserId == model.ShameUserId
            //                group v by v.Valeur
            //    into g
            //                select new { key = g.Key, count = g.Count() }).ToList();



            //model.VoteOui = compteur.Where(x => x.key).Sum(x => x.count);
            //model.VoteNon = compteur.Where(x => !x.key).Sum(x => x.count);

            //if (model.VoteOui > db.Users.Count(x => x.EquipeId == utilisateurConnecte.EquipeId) / 2)
            //{
            //    var shameUser = db.ShameUsers.Find(item.ShameUserId);
            //    shameUser.Statut = ShameUserStatut.valide;
            //}
            //else if (model.VoteNon > db.Users.Count(x => x.EquipeId == utilisateurConnecte.EquipeId) / 2)
            //{
            //    var shameUser = db.ShameUsers.Find(item.ShameUserId);
            //    shameUser.Statut = ShameUserStatut.refuse;
            //}
            //db.SaveChanges();

            //}

            //    return Ok(model);

        }

        // PUT: api/Votes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVote(int id, Vote vote)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vote.VoteId)
            {
                return BadRequest();
            }

            db.Entry(vote).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VoteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        //// POST: api/Votes
        //[ResponseType(typeof(Vote))]
        //public IHttpActionResult PostVote(VoteModel model)
        //{
        //    string currentUserId = User.Identity.GetUserId();
        //    User utilisateurConnecte = db.Users.Find(currentUserId);


        //    if (ModelState.IsValid)
        //    {

        //        var item = new Vote()
        //        {
        //            Valeur = model.Valeur,
        //            Createur = User.Identity.GetUserId(),
        //            ShameUserId = model.ShameUserId,
        //            DateCreation = DateTime.Now,
        //        };

        //        db.Votes.Add(item);
        //        db.SaveChanges();

        //        var compteur = (from v in db.Votes
        //                        where v.ShameUserId == model.ShameUserId
        //                        group v by v.Valeur
        //            into g
        //                        select new { key = g.Key, count = g.Count() }).ToList();



        //        model.VoteOui = compteur.Where(x => x.key).Sum(x => x.count);
        //        model.VoteNon = compteur.Where(x => !x.key).Sum(x => x.count);

        //        if (model.VoteOui > db.Users.Count(x => x.EquipeId == utilisateurConnecte.EquipeId) / 2)
        //        {
        //            var shameUser = db.ShameUsers.Find(item.ShameUserId);
        //            shameUser.Statut = ShameUserStatut.valide;
        //        }
        //        else if (model.VoteNon > db.Users.Count(x => x.EquipeId == utilisateurConnecte.EquipeId) / 2)
        //        {
        //            var shameUser = db.ShameUsers.Find(item.ShameUserId);
        //            shameUser.Statut = ShameUserStatut.refuse;
        //        }
        //        db.SaveChanges();
        //    }
        //    return Ok(model);
        //}

        // DELETE: api/Votes/5
        [ResponseType(typeof(Vote))]
        public IHttpActionResult DeleteVote(int id)
        {
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return NotFound();
            }

            db.Votes.Remove(vote);
            db.SaveChanges();

            return Ok(vote);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool VoteExists(int id)
        {
            return db.Votes.Count(e => e.VoteId == id) > 0;
        }
    }
}