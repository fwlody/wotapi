﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class EquipeUserController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        // GET: api/EquipeUser
        public IQueryable<EquipeUserModel> GetEquipeUserModels()
        {
            return db.EquipeUserModels;
        }

        // GET: api/EquipeUser/5
        [ResponseType(typeof(EquipeUserModel))]
        public IHttpActionResult GetEquipeUserModel(int id)
        {
            EquipeUserModel equipeUserModel = db.EquipeUserModels.Find(id);
            if (equipeUserModel == null)
            {
                return NotFound();
            }

            return Ok(equipeUserModel);
        }

        // PUT: api/EquipeUser/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEquipeUserModel(int id, EquipeUserModel equipeUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != equipeUserModel.EquipeUserId)
            {
                return BadRequest();
            }

            db.Entry(equipeUserModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EquipeUserModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EquipeUser
        [ResponseType(typeof(EquipeUserModel))]
        public IHttpActionResult PostEquipeUserModel(EquipeUserModel equipeUserModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EquipeUserModels.Add(equipeUserModel);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = equipeUserModel.EquipeUserId }, equipeUserModel);
        }

        // DELETE: api/EquipeUser/5
        [ResponseType(typeof(EquipeUserModel))]
        public IHttpActionResult DeleteEquipeUserModel(int id)
        {
            EquipeUserModel equipeUserModel = db.EquipeUserModels.Find(id);
            if (equipeUserModel == null)
            {
                return NotFound();
            }

            db.EquipeUserModels.Remove(equipeUserModel);
            db.SaveChanges();

            return Ok(equipeUserModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EquipeUserModelExists(int id)
        {
            return db.EquipeUserModels.Count(e => e.EquipeUserId == id) > 0;
        }
    }
}