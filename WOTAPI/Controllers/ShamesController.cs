﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class ShamesController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        // GET: api/Shames
        public List<Shame> GetShames()
        {
            var model = (from m in db.Shames
                         orderby m.EstActif descending
                         select m).ToList();
            return (model);
        }

        // GET: api/Shames/5
        [ResponseType(typeof(Shame))]
        public Shame GetShame(int id)
        {
            Shame shame = db.Shames.Find(id);
            if (shame == null)
            {
                return (null);
            }

            return shame;
        }
        
        // POST: api/Shames
        public HttpResponseMessage PostShame(CreateShameModel model)
        {
            byte[] imageConvertie = new byte[] { };

            if (model.Image != null)
            {
                using (var item = new MemoryStream())
                {
                    model.Image.InputStream.CopyTo(item);
                    imageConvertie = item.ToArray();
                }
            }
            if (ModelState.IsValid)
            {
                var shame = new Shame()
                {
                    Nom = model.Nom,
                    Description = model.Description,
                    Image = imageConvertie,
                    Date = DateTime.Now,
                    EstActif = true,
                };

                db.Shames.Add(shame);
                db.SaveChanges();
            }

            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
        /*
  
        }*/
        /*
         * 
        //GET
        public HttpResponseMessage GetAddShame()
        {
            string currentUserId = User.Identity.GetUserId();
            Users utilisateurConnecte = db.Users.Find(currentUserId);

            var model = new DashboardModel()
            {
                NomPeriode = db.Periodes.Where(x => DateTime.Now > x.DateDebut && DateTime.Now < x.DateFin).Select(y => y.NomPeriode).Single(),

            };

            var addShame = new AddShameUserModel()
            {
                ShameList = (from m in db.Shames
                             where m.EstActif
                             select m).ToList(),
                UserList = db.Users.Where(x => x.EquipeId == utilisateurConnecte.EquipeId).ToList(),
                DateAddShame = DateTime.Now

            };

            model.AddShameUserModel = addShame;
            return Request.CreateResponse(HttpStatusCode.OK, addShame);
        }*/

        // PUT: api/Shames/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PutShame(int id, Shame model)
        {
            var thisShame = db.Shames.Find(model.Id);

            if (ModelState.IsValid)
            {
                thisShame.Description = model.Description;
                thisShame.Nom = model.Nom;
                //thisShame.Image = imageConvertie.Length > 0 ? imageConvertie : thisShame.Image;
                thisShame.EstActif = model.EstActif;

                //db.Entry(shame).State = EntityState.Modified;
                db.SaveChanges();
            }
            return Ok(thisShame);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != model.Id)
            {
                return BadRequest();
            }

            db.Entry(model).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return (null);
        }

        // DELETE: api/Shames/5
        [ResponseType(typeof(Shame))]
        public HttpResponseMessage DeleteShame(int id)
        {
            Shame shame = db.Shames.Find(id);
            if (shame == null)
            {
                return (null);
            }

            db.Shames.Remove(shame);
            db.SaveChanges();

            return Request.CreateResponse(HttpStatusCode.OK, shame);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShameExists(int id)
        {
            return db.Shames.Count(e => e.Id == id) > 0;
        }
    }
}