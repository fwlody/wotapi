﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WOTAPI.Models;

namespace WOTAPI.Controllers
{
    public class HomeController : ApiController
    {
        private WOTAPIContext db = new WOTAPIContext();

        public ActionResult Index()
        {
            return (null);
        }
        public HttpResponseMessage GetPeriodName()
        {
            var model = new DashboardModel()
            {
                NomPeriode = db.Periodes.Where(x => DateTime.Now > x.DateDebut && DateTime.Now < x.DateFin).Select(y => y.NomPeriode).Single(),
            };
            return Request.CreateResponse(HttpStatusCode.OK, model);

        }

        public HttpResponseMessage GetPodium()
        {
            string currentUserId = User.Identity.GetUserId();
            User utilisateurConnecte = db.Users.Find(currentUserId);

            var equipeUsers = db.Users.Where(y => y.EquipeId == utilisateurConnecte.EquipeId).Select(y => y.Id).ToList();
            ;


            List<IGrouping<User, ShameUser>> groupementShamePerUser = db.ShameUsers.Where(z => z.Statut == ShameUserStatut.valide && equipeUsers.Contains(z.UserId)).GroupBy(x => x.User).OrderByDescending(y => y.Count()).Take(3).ToList();

            List<UserModel> userViewModels = groupementShamePerUser.Select(
                g =>
                    new UserModel
                    {
                        ShamesList =
                                g.Select(s => new ShameUserModel { ShameUserId = s.ShameUserId, Statut = s.Statut })
                                        .ToList(),

                        UserId = g.Key.Id,
                        UserName = g.Key.Prenom,

                    }).ToList();


            return Request.CreateResponse(HttpStatusCode.OK, userViewModels);
        }
    }
}
