namespace WOTAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EquipeUserModels",
                c => new
                    {
                        EquipeUserId = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.EquipeUserId);
            
            AddColumn("dbo.ShameUsers", "Justificatif", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ShameUsers", "Justificatif");
            DropTable("dbo.EquipeUserModels");
        }
    }
}
