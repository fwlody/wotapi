﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class ShameUser
    {
        public int ShameUserId { get; set; }
        public int ShameId { get; set; }
        [Column("User_Id")]
        public string UserId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateShame { get; set; }

        public string Createur { get; set; }
        public ShameUserStatut Statut { get; set; }
        public virtual Shame Shame{ get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
        public virtual List<Vote> Votes { get; set; }
        public virtual Periode Periode { get; set; }
        public string Justificatif { get; set; }

    }
    public enum ShameUserStatut 
    {
        propose,
        valide,
        refuse

    }
}