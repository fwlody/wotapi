﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class Periode
    {
        public int PeriodeId { get; set; }
        public string NomPeriode { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime DateDebut{ get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateFin { get; set; }

    }
}