﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class VoteModel
    {
        public bool Valeur { get; set; }
        public int VoteOui { get; set; }
        public int VoteNon { get; set; }
        public int ShameUserId { get; set; }
    }
}