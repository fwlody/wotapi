﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class Equipe
    {
        public int EquipeId { get; set; }
        public string NomEquipe { get; set; }
        public byte[] PhotoEquipe { get; set; }
        public List<User> User { get; set; }

    }
}