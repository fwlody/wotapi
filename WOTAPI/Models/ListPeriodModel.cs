using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class ListPeriodModel
    {

        public List<Periode> PeriodList { get; set; }

    }
}