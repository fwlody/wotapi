﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class AddShameUserModel
    {

        public string UserId { get; set; }
        public int ShameId { get; set; }

        [Display(Name = "Choisir Shame")]
        public List<Shame> ShameList { get; set; }

        [Display(Name = "Choisir Membre")]
        [JsonIgnore]
        public List<User> UserList { get; set; }
        public DateTime DateAddShame { get; set; }
        public string Justificatif { get; set; }
    }
}