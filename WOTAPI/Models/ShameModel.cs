﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class ShameModel
    {
        public string Description { get; set; }
        public string Nom { get; set; }
        public int Id { get; set; }

        public HttpPostedFileBase Image { get; set; }
        public byte[] ImageOriginal { get; set; }

        public DateTime Date { get; set; }
        public bool EstActif { get; set; }



    }
}