﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class ShameUserModel : ShameModel
    {
        public int ShameUserId { get; set; }
        public ShameUserStatut Statut { get; set; }
        public int VoteOui { get; set; }
        public int VoteNon { get; set; }
        public int Valides { get; set; }
        public int Proposes { get; set; }
        public int Refuses { get; set; }
        public int Total { get; set; }
        public string Justificatif { get; set; }

    }
}