﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class UserModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string UserLastName { get; set; }
        public string Justificatif { get; set; }
        public List<ShameUserModel> ShamesList { get; set; }
        public int Count => ShamesList.Count;
    }
}