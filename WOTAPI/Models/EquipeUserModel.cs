﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class EquipeUserModel
    {
        [Key]
        public int EquipeUserId { get; set; }
    }
}