﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class DashboardModel
    {
        public AddShameUserModel AddShameUserModel { get; set; }
        public UserModel UserViewModel { get; set; }
        public int PeriodeId { get; set; }
        public string NomPeriode { get; set; }
        public string Justificatif { get; set; }
    }
}