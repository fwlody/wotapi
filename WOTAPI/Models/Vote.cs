﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class Vote
    {
        public int VoteId { get; set; }
        public int ShameUserId { get; set; }
        public bool Valeur { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DateCreation { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Date { get; set; }
        public string Createur { get; set; }
        public virtual ShameUser ShameUser { get; set; }
        

    }
}