﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class JoinEquipeModel
    {
        public List<User> MesCoequipiers { get; set; }
        public List<Equipe> Equipes { get; set; }
        public int EquipeId { get; set; }
        public string UserId { get; set; }
        public bool EstDansUneEquipe { get; set; }
        public byte[] PhotoOriginal { get; set; }

    }
}
