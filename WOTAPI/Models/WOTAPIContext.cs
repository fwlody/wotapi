﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace WOTAPI.Models
{
    public class WOTAPIContext : IdentityDbContext<User>
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public WOTAPIContext() : base("name=WOTAPIContext")
        {
        }

        public virtual DbSet<Shame> Shames { get; set; }
        public virtual DbSet<ShameUser> ShameUsers { get; set; }
        public virtual DbSet<Vote> Votes { get; set; }
        public virtual DbSet<Equipe> Equipes { get; set; }
        public virtual DbSet<Periode> Periodes { get; set; }

        public System.Data.Entity.DbSet<WOTAPI.Models.EquipeUserModel> EquipeUserModels { get; set; }
    }

    public class User : IdentityUser
    {
        public string Nom { get; internal set; }
        public string Prenom { get; internal set; }
        public int? EquipeId { get; set; }
        [JsonIgnore]
        public virtual Equipe Equipe { get; set; }
        public virtual List<ShameUser> ShameUsers { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
