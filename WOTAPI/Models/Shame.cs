﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class Shame 
    {
        public string Description { get;  set; }
        public string Nom { get;  set; }
        public int Id { get; set; }

        public byte[] Image { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Date { get; set; }

        public bool EstActif { get; set; }





    }
}