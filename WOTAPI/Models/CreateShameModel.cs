﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WOTAPI.Models
{
    public class CreateShameModel
    {
        [Required]
        [Display(Name = "Nom")]
        public string Nom { get; set; }

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Required]
        public HttpPostedFileBase Image { get; set; }

        [Required]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        public object Id { get; internal set; }

    }
}